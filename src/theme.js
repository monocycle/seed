const { color, hsla } = require('csx/lib')


const theme = {
  colors: {

    // default: '#b8b8b8',
    primary: color('#009688'),
    invalid: color('#e93f3b'),
    valid: color('#1bc876'),
    light: hsla(0, 0, .86, 1),
    lightTransparent: hsla(0, 0, .86, .9),
    lighter: hsla(0, 0, .94, 1),
    darkTransparent: hsla(0, 0, 0, .2),
    dark: hsla(0, 0, .2, 1),
    darker: hsla(0, 0, .13, 1),
    blue: color('3c8ddc')
  }
}


// const transparent = color('trnsparent')
// const primary = color('#009688')
// const invalid = color('#e93f3b')
// const valid = color('#1bc876')
// const light = hsla(0, 0, .86, 1)
// const lightTransparent = hsla(0, 0, .86, .9)
// const lighter = hsla(0, 0, .94, 1)
// const darkTransparent = hsla(0, 0, 0, .2)
// const dark = hsla(0, 0, .2, 1)
// const darker = hsla(0, 0, .13, 1)
// const blue = color('3c8ddc')


// theme.colors.cardBackground = light
// theme.colors.cardText = dark

// theme.colors.fieldBackground = light
// theme.colors.fieldSeparator = darkTransparent
// theme.colors.fieldText = dark

// theme.colors.linkBackground = transparent
// theme.colors.linkText = primary

// theme.colors.buttonBackground = transparent
// theme.colors.buttonText = light
// defaultTheme.colors.cardBackground = defaultTheme.colors.dark.lighten(.1)

module.exports = {
  default: theme,
  defaultTheme: theme
}