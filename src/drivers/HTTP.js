const {makeHTTPDriver} = require('@cycle/http')

module.exports = makeHTTPDriver()
