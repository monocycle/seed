const pipe = require('ramda/src/pipe')
const { captureClicks } = require('@cycle/history/lib/cjs/captureClicks')
const { makeHistoryDriver, makeServerHistoryDriver } = require('@cycle/history/lib/cjs/drivers')
const withIsolatedHistory = require('../../drivers/IsolatedHistory')


const historyDriver = typeof window === 'undefined'
  ? makeServerHistoryDriver()
  : makeHistoryDriver()

module.exports = pipe(
  captureClicks,
  withIsolatedHistory
)(historyDriver)
