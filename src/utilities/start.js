const { setup } = require('@cycle/run')
const { rerunner, restartable } = require('cycle-restart')
const { withState } = require('@cycle/state')
const pipe = require('ramda/src/pipe')
const always = require('ramda/src/always')
const apply = require('ramda/src/apply')
const isUndefined = require('ramda-adjunct/lib/isUndefined').default
const isPlainObj = require('ramda-adjunct/lib/isPlainObj').default
const isFunction = require('ramda-adjunct/lib/isFunction').default
const over = require('ramda/src/over')
const mergeDeepRight = require('ramda/src/mergeDeepRight')
const identity = require('ramda/src/identity')
const assoc = require('ramda/src/assoc')
const tap = require('ramda/src/tap')
const __ = require('ramda/src/__')
const ifElse = require('ramda/src/ifElse')
const unless = require('ramda/src/unless')
const lensProp = require('ramda/src/lensProp')
const { coerce } = require('@monocycle/component/utilities/coerce')
const { WithSymbols } = require('@monocycle/symbols')
// const { ViewCombiner, DefaultView, mergeViewOptions } = require('@monocycle/dom/utilities')
const log = require('@monocycle/component/utilities/log').Log('Start')
const { WithIsolated } = require('@monocycle/isolated')
const { makeComponent } = require('@monocycle/component')
const { WithTransition } = require('@monocycle/state/Transition')
const { WithView } = require('@monocycle/dom/View')
const { WithDebug } = require('@monocycle/dom/Debug')
const { reinit } = require('typestyle/lib')

const Start = ({
  theme: defaultTheme,
  Behavior,
  Drivers,
  state,
  Style,
  dependencies = {},
  makeComponent: _makeComponent = makeComponent,
} = {}) => {

  if (!isFunction(Behavior))
    throw new Error(`'Behavior' must be a component behavior factory`)

  let _rerun = rerunner(
    setup,
    Drivers
  )

  const start = pipe(
    coerce,
    log.partial('start()'),
    over(lensProp('Component'), pipe(
      unless(isFunction, pipe(
        () => _makeComponent(),
        WithSymbols()
      )),
      // unless(isFunction, () => pipe(
      //   WithSymbols({
      //     mergeOptions: mergeViewOptions
      //   }),
      // )(makeComponent({
      //   Default: DefaultView,
      //   Combiners: options => ({
      //     DOM: ViewCombiner(options)
      //   }),
      //   operators: {
      //     isolate: WithIsolated
      //   }
      // })))
    )),
    over(lensProp('theme'),
      ifElse(isPlainObj,
        mergeDeepRight(defaultTheme),
        always(defaultTheme)
      )
    ),
    tap(({ Component, theme }) => {

      reinit()

      Style({
        ...theme
      })

      Component.set({
        Transition: WithTransition,
        View: [[WithView, { theme }]],
        Debug: [[WithDebug, { theme }]],
        ...dependencies
      })
    }),
    // log.partial(1),
    assoc('state', state),
    Behavior,
    apply(__, []),
    withState,
    _rerun
  )

  return start
}


const initHMR = (module, clearConsole) => {

  if (!module.hot)
    return

  module.hot.accept(err => {

    console.error(err)

    initHMR(clearConsole)
  })

  module.hot.dispose((err) => {
    // if (err)
    //   throw err
    clearConsole && console.clear()
  })
}

module.exports = {
  default: Start,
  Start,
  initHMR
}