const { cssRule } = require('typestyle/lib')
const { setupPage, normalize, attachToBottom, margin } = require('csstips')
const { rem } = require('csx/lib')

// #region old
// const definitions = [
//   {
//     name: 'App',
//     Styles: AppStyle,
//     colors: {
//       background: 'lightTransparent'
//     }
//   },
//   process.env.NODE_ENV !== 'production' && { name: 'Debug', Styles: DebugStyle, },
//   { name: 'Layout', Styles: LayoutStyle, },
//   {
//     name: 'Flexible',
//     Styles: () => [
//       flex,
//       {
//         $debugName: 'Flexible',
//       }
//     ]
//   },
//   {
//     name: 'Content',
//     Styles: () => [
//       content,
//       {
//         $debugName: 'Content',
//       }
//     ]
//   },
//   {
//     name: 'Bar',
//     Styles: BarStyle,
//     needs: ['Layout'],
//     colors: {
//       background: 'dark',
//     },
//     options: {

//     }
//   },
//   {
//     name: 'Navigation',
//     Styles: NavigationStyle,
//     colors: {
//       background: 'dark',
//     },
//   },
//   {
//     name: 'Link',
//     Styles: LinkStyle,
//     colors: {
//       default: 'light'
//     },
//     options: {
//       textDecoration: 'none'
//     }
//   },

//   { name: 'LinkList', Styles: LinkListStyle },
//   { name: 'Field', Styles: FieldStyle },
//   { name: 'FieldLabel', Styles: FieldLabelStyle },
//   { name: 'FieldInput', Styles: FieldInputStyle },
//   { name: 'FieldMessage', Styles: FieldMessageStyle },
//   { name: 'TextareaField', Styles: TextareaFieldStyle },
//   { name: 'SvgIcon', Styles: SvgIconStyle },

//   { name: 'Image', Styles: ImageStyle },
//   // { name: 'Button', Styles: ButtonStyle },
//   // { name: 'Codemirror', Styles: CodemirrorStyle },
//   // { name: 'Editor', Styles: EditorStyle },
//   {
//     name: 'Card',
//     Styles: CardStyle,
//     colors: {
//       background: 'lighter'
//     },
//     needs: ['Layout']
//   },
// ]
// #endregion

const Style = ({
  rootSelector = '.root',
  // debug = true,
  // colors = {},
} = {}) => {

  if (typeof window === 'undefined')
    return

  normalize()

  cssRule('*, h1, h2, h3, h4, h5, h6', margin(0))

  cssRule('html', {
    fontSize: '62.5%',
    height: 'initial'
  })

  cssRule('h1', {
    textAlign: 'center',
    margin: '2.68rem 0',

    // margin: 
  })

  // cssRule('h1, h2', {
  //   color: '#ff9e4d;'
  // })

  cssRule('body', {
    // color: colors.light.toString(),
    fontSize: rem(2),
  })

  cssRule('address', {
    fontStyle: 'normal',
  })

  // cssRule('.container', {
  //   color: colors.dark.toString(),
  // })

  cssRule('.important', {
    '&': {
      position: 'relative',
      '&::after': {
        content: '""',
        ...attachToBottom,
        display: 'block',
        height: '3px',
        // backgroundColor: '#ff9e4d',
      }
    },
    // '&> ::after': {
    //   content: '',
    //   position: 'absolute',
    //   left: 0,
    //   right: 0,
    //   bottom: 0,
    //   height: '2px',
    //   backgroundColor: '#ff9e4d'
    // }
  })

  cssRule('strong', {
    // display: 'block',
    // borderBottom: '2px solid #ff9e4d'
  })

  setupPage(rootSelector)
}

module.exports = {
  default: Style,
  Style,
}