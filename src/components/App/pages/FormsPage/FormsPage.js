
const pipe = require('ramda/src/pipe')
const tap = require('ramda/src/tap')
const path = require('ramda/src/path')
const applyTo = require('ramda/src/applyTo')
const prop = require('ramda/src/prop')
const { ensurePlainObj } = require('@monocycle/component/utilities/ensurePlainObj')
const { WithLayout } = require('@monocycle/dom/Layout')
const { WithView } = require('@monocycle/dom/View')
const { WithBar } = require('@monocycle/dom/Bar')
const { WithContactForm } = require('./ContactForm')
// const { WithGroupExample } = require('../../../GroupExample')
const over = require('ramda/src/over')
const lensProp = require('ramda/src/lensProp')
const unless = require('ramda/src/unless')
const isFunction = require('ramda-adjunct/lib/isFunction').default
const { makeComponent } = require('@monocycle/component')
const { WithSymbols } = require('@monocycle/symbols')
const { CardStyle } = require('@monocycle/dom/Card/style')
const { BarStyle } = require('@monocycle/dom/Bar/style')
const { style } = require('typestyle')
// const log = require('@monocycle/component/utilities/log').Log('FormsPage')


const WithFormsPage = pipe(
  ensurePlainObj,
  over(lensProp('Component'), pipe(
    unless(isFunction, pipe(
      makeComponent.bind(void 0, void 0),
      WithSymbols()
    ))
  )),
  // log.partial(1),
  tap(({ Component: C, theme }) => C.set({
    View: WithView,
    H1View: [['View', { sel: 'h1' }]],
    H2View: [['View', { sel: 'h2' }]],
    ParagraphView: [['View', { sel: 'p' }]],
    FlexibleView: [['View', { sel: '.Flexible' }]],
    ListView: [['View', { sel: 'ul' }]],
    ListItemView: [['View', { sel: 'li' }]],
    Layout: WithLayout,
    // BarLayout: [['Layout', { sel: '.Bar' }]],
    CardLayout: [['Layout', { sel: '.' + style(...CardStyle(theme)) }]],
    // GroupExample: [[WithGroupExample, { theme }]],
    Bar: [[WithBar, { theme }]],

    ContactForm: [[WithContactForm, { theme }]],
  })),
  ({ Component: C }) => {

    const Layout = C.get('Layout').make
    const CardLayout = C.get('CardLayout').make
    const BarLayout = C.get('Bar').make

    const FlexibleView = C.get('FlexibleView').make
    const H1 = C.get('H1View').make
    const H2 = C.get('H2View').make
    const P = C.get('ParagraphView').make
    const List = C.get('ListView').make
    const Item = C.get('ListItemView').make

    // const GroupExample = C.get('GroupExample').make

    const ContactForm = C.get('ContactForm').make

    return component => C(component)
      .concat(Layout({
        spaced: true,
        fill: true,
        direction: 'column',
        has: [
          H1('Forms'),
          Layout({
            style: {
              justifyContent: 'center',
              alignItems: 'center',
            },
            has: [
              FlexibleView([

                CardLayout({
                  has: [
                    P(`Components/Behaviors used:`),
                    List([
                      Item('Form'),
                      Item('FieldGroup'),
                      Item('Field'),
                      Item('Button'),
                      Item('Clickable'),
                      Item('Validable'),
                      Item('Focusable'),
                      Item('Codemirror'),
                      Item('Editor'),
                      Item('(to be continued)'),
                    ]),
                  ]
                }).map(C.get('Layout', {
                  direction: 'column',
                  spaced: true,
                })),

                H2('Codemirror'),

                CardLayout({
                  gutter: false,
                  has: [

                    '<Codemirror>'
                    // makeCodemirror().isolation({
                    //   DOM: 'Codemirror',
                    //   '*': null,
                    // })
                  ]
                }),

                H2('Editor'),

                CardLayout({
                  gutter: false,
                  has: [
                    '<Editor>'
                    // makeEditor({ classes }).isolation({
                    //   DOM: 'Editor',
                    //   '*': null,
                    // })
                  ]
                }),
              ]).map(C.get('Layout', {
                direction: 'column',
                spaced: true
              })),

              FlexibleView({
                has: [
                  H2('Contact form'),
                  CardLayout({
                    gutter: false,
                    has: FlexibleView({
                      sel: '.contact-form',
                      has: [
                        BarLayout({
                          from: (sinks, sources) => sources.state.stream
                            .map(prop('message'))
                            .startWith('')
                            .map(message => ({
                              style: {
                                justifyContent: 'center',
                                display: !message ? 'none' : 'flex',
                                backgroundColor: message && message.type === 'success'
                                  ? '#1bc876'
                                  : '#e93f3b',
                                color: '#fefefe'
                              },
                              children: message
                            }))
                        }),
                        ContactForm({
                          key: 'contactForm'
                        }),
                      ]
                    })
                  })
                ]
              }).map(C.get('Layout', {
                direction: 'column',
                spaced: true
              })),
            ]
          }),
        ]
      }))
    // .isolate('formsPage')
  }
)

module.exports = {
  default: WithFormsPage,
  WithFormsPage
}

