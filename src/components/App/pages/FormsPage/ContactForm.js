
const { Stream: $ } = require('xstream')
const { makeComponent } = require('@monocycle/component')
const { coerce } = require('@monocycle/component/utilities/coerce')
// const { makeField } = require('@monocycle-dom/Field')

const pipe = require('ramda/src/pipe')
const dropRepeats = require('xstream/extra/dropRepeats').default
const prop = require('ramda/src/prop')
const over = require('ramda/src/over')
const path = require('ramda/src/path')
const identical = require('ramda/src/identical')
const always = require('ramda/src/always')
const tap = require('ramda/src/tap')
const assoc = require('ramda/src/assoc')
const when = require('ramda/src/when')
const unless = require('ramda/src/unless')
const objOf = require('ramda/src/objOf')
const lensProp = require('ramda/src/lensProp')
const { WithSymbols } = require('@monocycle/symbols')
// const { EmptyObject } = require('@monocycle/component/utilities/empty')
// const isPlainObj = require('ramda-adjunct/lib/isPlainObj').default
const isFunction = require('ramda-adjunct/lib/isFunction').default
const isString = require('ramda-adjunct/lib/isString').default
const isNonEmptyString = require('ramda-adjunct/lib/isNonEmptyString').default
const { Rule } = require('@monocycle/validable/Rule')
const { WithTextareaField } = require('@monocycle/dom/TextareaField')
const lt = require('ramda/src/lt')
const both = require('ramda/src/both')
const propEq = require('ramda/src/propEq')
const test = require('ramda/src/test')
const { WithField } = require('@monocycle/dom/Field')
const { WithForm } = require('@monocycle/dom/Form')
const { WithListener } = require('@monocycle/listener')
const { WithTransition } = require('@monocycle/state/Transition')
const { WithView } = require('@monocycle/dom/View')
// const { WithButton } = require('@monocycle/dom/Button')
const { WithBar } = require('@monocycle/dom/Bar')
const { WithIsolated } = require('@monocycle/isolated')
const log = require('@monocycle/component/utilities/log').Log('ContactForm')
const { Log } = require('@monocycle/component/utilities/log')
const { ViewCombiner, DefaultView, mergeViewOptions } = require('@monocycle/dom/utilities')
const { WithDebug, WithStateDebug } = require('@monocycle/dom/Debug')
const { div } = require('@cycle/dom')
const { ensurePlainObj } = require('@monocycle/component/utilities/ensurePlainObj')

const phoneRegex = /^\d+$/
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

// #region old
const WithContactFormOld = pipe(
  coerce,
  over(lensProp('Component'), pipe(
    unless(isFunction, pipe(
      makeComponent.bind(void 0, {
        operators: {
          isolate: WithIsolated
        }
      }),
      WithSymbols()
    ))
  )),
  log.partial(1),
  tap(({ Component, theme }) => Component.set({
    // C.set('Isolated', WithIsolated)
    // Listener: WithListener,
    Transition: [[WithTransition, {
      Log: name => Log(name)
        .bind(void 0, `%c:`, ['color: #32b87c'].join(';')),
    }]],
    Field: [[WithField, { theme }]],
    // TextareaField: [[WithTextareaField, { theme }]],
    Form: [[WithForm, { theme }]],
    // View: WithView,
    // Layout: WithLayout,
    // Bar: [[WithBar, { theme }]],
    // Button: [[WithButton, { theme }]],
    // FlexibleView: [['View', { sel: '.' + style(...FlexibleStyle(theme)) }]],
    // BarLayout: [['Layout', { sel: '.' + style(...BarStyle(theme)) }]],
  })),
  // over(lensProp('requestOptions'), pipe(
  //   unless(isPlainObj, EmptyObject),
  //   over(lensProp('url'), pipe(
  //     unless(isNonEmptyString, always('contact'))
  //   )),
  //   over(lensProp('category'), pipe(
  //     unless(isNonEmptyString, always('sendMessage'))
  //   ))
  // )),
  // over(lensProp('key'),
  //   unless(isNonEmptyString, () => 'contactForm' + Math.random().toString(36).substring(7))
  // ),
  ({ Component, has, classes, ...formOptions }) => {

    const WithForm = Component.get('Form')
    // const WithButton = Component.get('Button')
    const Field = Component.get('Field').make
    // const FlexibleView = Component.get('FlexibleView').make
    // const Bar = Component.get('Bar').make

    // return component => Component(component)
    //   .map(WithForm({
    //     ...formOptions,

    //   }))

    return component => Component(component)
      // .map(C.get('Listener', {
      //   from: (sinks, sources) => sources.HTTP.select('sendMessage')
      //     .debug('response$')
      //     .map(response$ => response$.replaceError(pipe(prop('response'), $.of)))
      //     .flatten()
      //     .debug('response'),
      //   combiner: $.merge,
      //   to: 'sendMessageResponse$'
      // }))

      .map(Component.get('Listener', {
        from: (sinks, sources) => sources.state.stream
          .filter(prop('isForm'))
          .map(prop('submittedAt'))
          .filter(Boolean)
          .compose(dropRepeats())
          .debug('submittedAt')
          .mapTo({ statusCode: 200, message: 'sent' }),
        combiner: $.merge,
        to: 'sendUnicornResponse$'
        // to: 'resetForm$'
      }))
      .map(Component.get('Listener', {
        from: 'sendUnicornResponse$',
        to: 'resetForm$'
      }))
      .map(Component.get('Transition', {
        name: 'setMessage',
        from: (sinks, sources) =>
          sinks.sendUnicornResponse$
            .map(({ statusCode, message }) => {

              return $.merge(
                $.of(
                  statusCode === 200 && message === 'sent'
                    ? {
                      type: 'success',
                      text: 'Unicorn has been sent'
                    }
                    : (
                      statusCode === 400 && message === 'invalid from'
                        ? {
                          type: 'error',
                          text: 'Invalid email'
                        }
                        : {
                          type: 'error',
                          text: 'An error occured'
                        }
                    )
                ),
                $.of(void 0).compose(sources.Time.delay(4000))
              )
            })
            .flatten(),
        reducer: message => (state = {}) => ({
          ...state,
          message
        })
      }))
      // .map(C.get('Listener', {
      //   from: 'sendMessageResponse$',
      //   to: 'resetForm$'
      // }))
      .map(Component.get('Form', {
        ...formOptions,
        has: [
          {
            key: 'email',
            validate: Rule({
              error: {
                message: `Invalid email`,
              },
              validate: both(
                pipe(prop('length'), lt(6)),
                test(emailRegex)
              ),
            }),
          },
          {
            key: 'subject',
            // validate: AllRule([
            //   {
            //     validate: x => {

            //       return (+x) > 4
            //     },
            //     error: {
            //       message: 'err1'
            //     }
            //   },
            //   {
            //     validate: x => (+x) > 6,
            //     error: {
            //       message: 'err2'
            //     }
            //   },
            // ])
          },

          // C.get('Field').make({
          //   label: 'Email',
          //   name: 'email',
          //   validableOptions: {
          //     lens: lensProp('email'),
          //     message: `Invalid email`,
          //     validate: both(
          //       pipe(prop('length'), lt(1)),
          //       test(emailRegex)
          //     ),
          //   }
          // }),

          Component.get('TextareaField').make({
            label: 'Unicorn',
            key: 'content',
            validate: Rule({
              validate: pipe(prop('length'), lt(5)),
              error: {
                message: `This field is required`,
              }
            }),
          }),
        ]
      }))
    // .map(C.get('Transition', always(state => {

    //   console.log('init', { state })

    //   return state || {}
    // })))

  }
)
// #endregion

const WithRequest = pipe(
  ensurePlainObj,
  over(lensProp('Component'), pipe(
    unless(isFunction, pipe(
      makeComponent.bind(void 0, {}),
      WithSymbols()
    ))
  )),
  // over(lensProp('url')),
  ({ Component }) => {


    return component => Component(component)
      .map(WithListener({
        from: 'sendRequest$',
        to: 'HTTP'
      }))
  }
)

const WithContactForm = pipe(
  coerce,
  over(lensProp('Component'), pipe(
    unless(isFunction, pipe(
      makeComponent.bind(void 0, {
        Default: DefaultView,
        Combiners: options => ({
          DOM: ViewCombiner(options)
        }),
        operators: {
          isolate: WithIsolated
        }
      }),
      WithSymbols({
        mergeOptions: mergeViewOptions
      })
    ))
  )),

  tap(({ Component, theme }) => Component.set({
    View: WithView,
    Field: [[WithField, { theme }]],
    Form: [[WithForm, { theme }]],
    StateDebug: [[WithStateDebug, { theme }]],
    TextareaField: [[WithTextareaField, { theme }]],
    Bar: [[WithBar, { theme }]],
    // FlexibleView: [['View', { sel: '.' + style(...FlexibleStyle(theme)) }]],
    // FormView: [['View', { theme, sel: 'form' }]],
    Transition: [[WithTransition, {
      Log: name => Log(name)
        .bind(void 0, `%c:`, ['color: #32b87c'].join(';')),
    }]],
    // ValidableGroup: WithValidableGroup,
    // Button: [[WithButton, { theme }]],
  })),
  ({ Component, has, ...formOptions }) => {

    const View = Component.get('View').make
    const WithForm = Component.get('Form')
    const Bar = Component.get('Bar').make
    const Field = Component.get('Field').make
    const WithField = Component.get('Field')
    const StateDebug = Component.get('StateDebug').make
    const TextareaField = Component.get('TextareaField').make

    const TransitionLog = name => log.Log(name)
      .bind(void 0, `%c:`, ['color: #32b87c'].join(';'))

    const withContactForm = component => Component(component)
      .map(WithListener({
        from: (sinks, sources) => {
          return sources.HTTP.select('sendUnicorn')
            // .debug('response$')
            // .map(response$ => response$.replaceError(pipe(prop('response'), $.of)))
            .flatten()
            .replaceError(err => console.error(err))
        },
        to: 'response$'
      }))
      .map(WithListener({
        from: (sinks) => sinks.response$
          .filter(propEq('statusCode', 200))
          .map(path(['body', 'message']))
          .filter(identical('Unicorn sent successfully')),
        to: 'responseSuccess$'
      }))
      .map(WithListener({
        from: 'responseSuccess$',
        combine: $.merge,
        to: 'resetForm$'
      }))
      .map(WithForm({
        ...formOptions,
        has: {
          // message: Bar({
          //   from: (sinks, sources) => sources.state.stream
          //     .map(prop('message'))
          //     .debug('alertState'),
          //   has: 'pouet'
          // }),
          email: {
            validate: x => (x && x.length > 3)
              ? void 0
              : 'pouet'
          },
          subject: {
            validate: x => (x && x.length > 4)
              ? void 0
              : 'erf'
          },
          unicorn: TextareaField({
            label: 'Unicorn',
            key: 'content',
            validate: Rule({
              validate: pipe(prop('length'), lt(5)),
              error: {
                message: `This field is required`,
              }
            }),
          }),
        },
      }))
      .map(WithTransition({
        name: 'setMessage',
        Log: TransitionLog,
        from: (sinks, sources) =>
          sinks.responseSuccess$
            // .map(pipe(
            //   objOf('text'),
            //   assoc('type', 'success')
            // ))
            .map(message => {
              return $.merge(
                $.of(message),
                $.of('').compose(sources.Time.delay(4000))
              )
            })

            // .map(message => {

            //   return $.merge(
            //     $.of(
            //       statusCode === 200 && message === 'message sent successfully'
            //         ? {
            //           type: 'success',
            //           text: message
            //         }
            //         : (
            //           statusCode === 400 && message === 'invalid from'
            //             ? {
            //               type: 'error',
            //               text: 'Invalid email'
            //             }
            //             : {
            //               type: 'error',
            //               text: 'An error occured'
            //             }
            //         )
            //     ),
            //     $.of(void 0).compose(sources.Time.delay(4000))
            //   )
            // })
            .flatten()
            .debug('setMessage'),
        reducer: message => (state = {}) => ({
          ...state,
          message
        })
      }))

      .map(WithListener({
        from: (sinks, sources) => {
          return sinks.submitted$
            .map(({ when, what }) => ({
              method: 'post',
              url: 'unicorn',
              send: what,
              category: 'sendUnicorn',
            }))
            .debug('submitted')
            .replaceError(err => console.error(err))
        },
        to: 'HTTP'
      }))
      .isolate('contactForm')

    return component => Component({
      View: div,
      has: [
        Bar({
          from: (sinks, sources) => sources.state.stream
            .map(prop('contactForm'))
            .map(prop('message'))
            .compose(dropRepeats())
            .debug('alertState1')

            // .filter(Boolean)
            .map(when(Boolean, pipe(
              when(isString, objOf('text')),
              over(lensProp('type'), unless(isNonEmptyString, always('success'))),
            )))
            .debug('alertState2')

            .map(message => ({
              style: {
                justifyContent: 'center',
                display: !message ? 'none' : 'flex',
                backgroundColor: message && message.type === 'success'
                  ? '#1bc876'
                  : '#e93f3b',
                color: '#fefefe'
              },
              children: message && message.text
            }))
        }),
        withContactForm(component)
      ]
    })
    // .map(withContactForm)
  }
)


module.exports = {
  default: WithContactForm,
  WithContactForm
}
