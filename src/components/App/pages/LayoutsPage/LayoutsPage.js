

const { Stream: $ } = require('xstream')
const { footer } = require('@cycle/dom')
const pipe = require('ramda/src/pipe')
const tap = require('ramda/src/tap')
const path = require('ramda/src/path')
const applyTo = require('ramda/src/applyTo')
const over = require('ramda/src/over')
const lensProp = require('ramda/src/lensProp')
const { ensurePlainObj } = require('@monocycle/component/utilities/ensurePlainObj')
const { WithLayout } = require('@monocycle/dom/Layout')
const { WithView } = require('@monocycle/dom/View')
const { CardStyle } = require('@monocycle/dom/Card/style')
const { WithSymbols } = require('@monocycle/symbols')
const isFunction = require('ramda-adjunct/lib/isFunction').default
const unless = require('ramda/src/unless')
const { style } = require('typestyle')
const { makeComponent } = require('@monocycle/component')
// const log = require('@monocycle/component/utilities/log').Log('LayoutsPage')

const Footer = () => ({ DOM: $.of(footer()) })

const WithLayoutsPage = pipe(
  ensurePlainObj,
  over(lensProp('Component'), pipe(
    unless(isFunction, pipe(
      makeComponent.bind(void 0, void 0),
      WithSymbols()
    ))
  )),
  // log.partial(1),
  tap(({ Component: C, theme }) => C.set({
    View: WithView,
    ParagraphView: [['View', { sel: 'p' }]],
    Layout: WithLayout,
    CardLayout: [['Layout', { sel: '.' + style(...CardStyle(theme)) }]],
    FlexibleView: [['View', { sel: '.Flexible' }]],
  })),
  ({ Component: C }) => {

    const Layout = C.get('Layout').make
    const Card = C.get('CardLayout').make
    const P = C.get('ParagraphView').make
    const WithFlexibleView = C.get('FlexibleView')

    // const BarLayout = C.get('BarLayout').make


    return component => C(component)
      .concat(Layout({
        direction: 'column',
        gutter: false,
        fill: true,
        has: Layout({
          fill: true,
          has: Layout({
            direction: 'column',
            gutter: false,
            spaced: true,
            fill: true,
            has: [
              Card({
                sel: 'span',
                has: 'bu'
              }),
              Card({
                has: P(`Quae dum ita struuntur, indicatum est apud Tyrum indumentum regale textum occulte`),
              }),
              Card({
                sel: 'span',
                style: {
                  backgroundColor: 'red'
                },
                has: 'zom'
              }),
              Card({
                direction: 'row',
                sel: 'span',
                spaced: true,
                has: [
                  Card({
                    sel: 'span',
                    has: 'content'
                  }),
                  Card({
                    sel: 'span',
                    has: 'content'
                  }),
                  Card({
                    sel: 'span',
                    has: 'flexible'
                  }).map(WithFlexibleView()),
                  Card({
                    sel: 'span',
                    has: 'content'
                  })
                ]
              }),

              Layout({
                gutter: false,
                spaced: true,
                has: [
                  Card({
                    sel: 'span',
                    style: {
                      backgroundColor: '#bada55'
                    },
                    has: 'meu'
                  }).map(WithFlexibleView({
                    grow: 2
                  })),
                  Card({
                    sel: 'span',
                    style: {
                      backgroundColor: '#bada55'
                    },
                    has: 'meu'
                  }).map(WithFlexibleView()),
                ]
              }),

              Card({
                // direction: 'row',
                sel: 'span',
                spaced: true,
                has: [
                  Card({
                    sel: 'span',
                    has: 'ga'
                  }),
                  Card({
                    sel: 'span',
                    has: 'ga'
                  }),
                  Card({
                    sel: 'span',
                    has: 'ga'
                  }).map(WithFlexibleView()),

                  Card({
                    sel: 'span',
                    has: 'ga'
                  })
                ]
              }),
            ]
          })
        })
      }))
  }
)

module.exports = {
  default: WithLayoutsPage,
  WithLayoutsPage
}

