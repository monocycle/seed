const { Stream: $ } = require('xstream')
const { footer } = require('@cycle/dom')
const pipe = require('ramda/src/pipe')
const tap = require('ramda/src/tap')
const { ensurePlainObj } = require('@monocycle/component/utilities/ensurePlainObj')
const { WithLayout } = require('@monocycle/dom/Layout')

const Footer = () => ({ DOM: $.of(footer()) })

const WithAppFoot = pipe(
  ensurePlainObj,
  tap(({ Component: C }) => {

    C.set({
      Layout: WithLayout,
      BarLayout: [['Layout', { sel: '.Bar' }]],
    })
  }),
  ({ Component: C }) => {

    const BarLayout = C.get('BarLayout').make

    return component => C(component)
      .concat(BarLayout({
        style: {
          marginTop: 'auto',
          justifyContent: 'center',
          fontSize: '12px',
          fontFamily: 'initial',
        },
        has: 'Tous droits réservés - n00sphere labs',
      }).concat(Footer))
  }
)

module.exports = {
  default: WithAppFoot,
  WithAppFoot
}