const { style } = require('typestyle')
const { makeComponent } = require('@monocycle/component')
const { ViewCombiner, DefaultView, mergeViewOptions } = require('@monocycle/dom/utilities')
const { WithIsolated } = require('@monocycle/isolated')
const pipe = require('ramda/src/pipe')
const defaultTo = require('ramda/src/defaultTo')
const over = require('ramda/src/over')
const prop = require('ramda/src/prop')
const always = require('ramda/src/always')
const tap = require('ramda/src/tap')
const unless = require('ramda/src/unless')
const lensProp = require('ramda/src/lensProp')
const { ensurePlainObj } = require('@monocycle/component/utilities/ensurePlainObj')
const isFunction = require('ramda-adjunct/lib/isFunction').default
const { WithSymbols } = require('@monocycle/symbols')
const { WithLayout } = require('@monocycle/dom/Layout')
const { WithView } = require('@monocycle/dom/View')
const { WithRouter } = require('@monocycle/history/Router')
const PathToRegexp = require('path-to-regexp')
const { WithAppNav } = require('./Nav')
const { WithAppFoot } = require('./Foot')
const { AppStyle } = require('./style')
const { WithLayoutsPage } = require('./pages/LayoutsPage')
const { WithFormsPage } = require('./pages/FormsPage')
const { WithTransition } = require('@monocycle/state/Transition')
const { flex } = require('csstips/lib/flex')
const { WithDebug, WithStateDebug } = require('@monocycle/dom/Debug')
const { div } = require('@cycle/dom')
const log = require('@monocycle/component/utilities/log').Log('App')


const WithApp = pipe(
  log.partial('WithApp()'),
  ensurePlainObj,
  over(lensProp('Component'), pipe(
    unless(isFunction, () => pipe(
      WithSymbols({
        mergeOptions: mergeViewOptions
      }),
    )(makeComponent({
      Default: DefaultView,
      Combiners: options => ({
        DOM: ViewCombiner(options)
      }),
      operators: {
        isolate: WithIsolated
      }
    })))
  )),
  // log.partial(1),
  over(lensProp('theme'), ensurePlainObj),
  over(lensProp('Style'), unless(isFunction, always(AppStyle))),

  tap(({ Component: C, Style, theme }) => C.set({
    View: WithView,
    AppView: [['View', {
      sel: '.' + style(...Style(theme))
    }]],
    FlexibleView: [['View', {
      sel: '.' + style(flex, {
        $debugName: 'Flexible'
      })
    }]],
    Header: [['View', { sel: 'header' }]],
    Footer: [['View', { sel: 'footer' }]],
    StateDebug: [[WithStateDebug, { theme }]],

    // FlexibleView: [['View', { sel: '.Flexible' }]],
    Router: WithRouter,
    Layout: [[WithLayout, { theme }]],
    AppNavigation: [[WithAppNav, { theme }]],
    AppFoot: WithAppFoot,
    LayoutsPage: [[WithLayoutsPage, { theme }]],
    FormsPage: [[WithFormsPage, { theme }]],
  })),
  ({ Component: C }) => {

    const Layout = C.get('Layout').make
    const WithFlexible = C.get('FlexibleView')
    const Flexible = C.get('FlexibleView').make
    const Router = C.get('Router').make
    const Navigation = C.get('AppNavigation').make
    const Foot = C.get('AppFoot').make
    const LayoutsPage = C.get('LayoutsPage').make
    const FormsPage = C.get('FormsPage').make
    const Header = C.get('Header').make
    const Footer = C.get('Footer').make
    const StateDebug = C.get('StateDebug').make

    return component => C([Layout({
      gutter: false,
      fill: true,
      direction: 'column',
      has: [
        Header(Navigation()),
        Flexible({
          style: {
            overflowY: 'auto'
          },
          sel: '.container',
          has: [
            Router({
              Default: Layout({
                fill: true,
                has: 'This is home pageuh'
              }),
              resolve: [
                {
                  resolve: PathToRegexp('/layouts/:path?'),
                  // value: LayoutsPage().isolate('layoutsPage')
                  value: LayoutsPage()
                },
                {
                  resolve: PathToRegexp('/forms/:path?'),
                  value: FormsPage()
                },
              ]
            }).map(WithFlexible()),
          ]
        }),
        // Footer(Navigation()),

      ]
    })
      .map(C.get('AppView', void 0))
    ])
      .map(C.get('Listener', {
        from: (sinks, sources) => {

          return sources.state.stream.debug('STATE')
        }
      }))
    // .concat(StateDebug(), { View: div })
  }
)

module.exports = {
  default: WithApp,
  WithApp
}