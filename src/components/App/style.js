const lensProp = require('ramda/src/lensProp')
const { toString } = require('@monocycle/component/utilities/toString')
const { ensurePlainObj } = require('@monocycle/component/utilities/ensurePlainObj')
const pipe = require('ramda/src/pipe')
const over = require('ramda/src/over')
const merge = require('ramda/src/merge')

const { scrollY, vertical, flex, layerParent, fillParent } = require('csstips/lib')


const defaultColors = {
  appText: '#2a2a2a',
  appBackground: '#f2f2f2',
}


const AppStyle = pipe(
  ensurePlainObj,
  over(lensProp('colors'), pipe(
    ensurePlainObj,
    merge(defaultColors),
  )),
  ({
    colors = {}
  } = {}) => {
    console.log('AppStyle()', { colors })

    return [
      // padding(em(2), em(8)),
      vertical,
      // horizontal,
      flex,
      fillParent,
      layerParent,
      {
        $debugName: 'App',
        color: colors.default,
        fontSize: '2.2rem',
        color: toString(colors.appText),
        backgroundColor: toString(colors.appBackground),

        '& > .container': {
          // backgroundColor: colors.background,
          color: toString(colors.light),
          backgroundColor: toString(colors.dark),
          backgroundPosition: 'center',
          // ...fillParent,

          minHeight: 'calc(100vh - 109px)',
          flexBasis: 'inherit',

          // background: 'red'
          // ...scrollY,
          // minHeight: '40vh',
          // marginTop: 'calc(60vh + 11rem)',
          // maxWidth: '120rem',
          // margin: '0 auto 8rem',
          // '& > *': {
          //   // background: 'red'
          //   // ...scrollY
          //   // padding: '2.4rem',
          // },
        },
        '& .banner': {
          // ...pageTop,
          height: '100vh',
          width: '100%',
          position: 'fixed',
          // backgroundPosition: 'center 11rem',
          // backgroundSize: 'auto 40rem',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          marginTop: '11rem'
        },
        '& h2': {
          marginBottom: '2rem',
          // fontSize: '2.4rem'

        },
        '& small': {
          fontSize: '1.5rem'
        },
        //   backgroundSize: 'contain',
        // backgroundRepeat: 'no-repeat',
        // ...layerParent,

        //   backgroundAttachment: 'fixed',
        //   backgroundPosition: '0 8em',

        //   // background
        //   // '& > img': {
        //   //   maxHeight: viewHeight(42)
        //   // }
        //   ...verticallySpaced(em(1))
      }
    ]
  }
)


module.exports = {
  default: AppStyle,
  AppStyle
}