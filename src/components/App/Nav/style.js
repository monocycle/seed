const pipe = require('ramda/src/pipe')
const over = require('ramda/src/over')
const merge = require('ramda/src/merge')
const lensProp = require('ramda/src/lensProp')
const { toString } = require('@monocycle/component/utilities/toString')
const { ensurePlainObj } = require('@monocycle/component/utilities/ensurePlainObj')

const { important } = require('csx/lib/strings')
const { rem } = require('csx/lib/units')
const { padding, fillParent, flexRoot, height, content, flex, horizontal } = require('csstips/lib')

const defaultColors = {
  appNavText: '#eee',
  appNavBackground: '#2a2a2a',
}


const AppNavStyle = pipe(
  ensurePlainObj,
  over(lensProp('colors'), pipe(
    ensurePlainObj,
    merge(defaultColors),
  )),
  over(lensProp('colors'), pipe(
    ensurePlainObj,
    merge(defaultColors),
  )),
  ({ colors }) => {

    return [
      {
        $debugName: 'Navigation',
        backgroundColor: toString(colors.appNavBackground),
        // color: important(toString(colors.appNavText)),
        ...height(rem(6.4)),

        '& > a': {
          ...padding(rem(.8)),
          '& > img': {
            ...height('100%'),
          },
        },
        '& > ul': {
          ...horizontal,
          // ...content,
          // ...height('100%'),
          listStyle: 'none',
          ...padding(0),
          '& > li': {
            ...flexRoot,
            // ...fillParent,
            // justifyContent: 'center',
            // alignItems: 'center',
            ...height('100%'),

            '& a': {
              ...fillParent,
              ...flexRoot,
              justifyContent: 'center',
              alignItems: 'center',
              textDecoration: 'none',
              color: important(toString(colors.appNavText)),
            },
          }
        }
      }
    ]
  }
)

module.exports = {
  default: AppNavStyle,
  AppNavStyle
}
