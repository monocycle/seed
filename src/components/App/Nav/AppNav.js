const pipe = require('ramda/src/pipe')
const tap = require('ramda/src/tap')
const over = require('ramda/src/over')
const unless = require('ramda/src/unless')
const lensProp = require('ramda/src/lensProp')
const { ensurePlainObj } = require('@monocycle/component/utilities/ensurePlainObj')
const { WithLayout } = require('@monocycle/dom/Layout')
const { WithView } = require('@monocycle/dom/View')
const { AppNavStyle } = require('./style')
const { WithLink } = require('@monocycle/dom/Link')
const always = require('ramda/src/always')
const { style } = require('typestyle')
const isFunction = require('ramda-adjunct/lib/isFunction').default
// const log = require('@monocycle/component/utilities/log').Log('AppNav')

const WithAppNav = pipe(
  ensurePlainObj,
  over(lensProp('Style'), unless(isFunction, always(AppNavStyle))),
  over(lensProp('theme'), ensurePlainObj),
  tap(({ Component, Style, theme }) => Component.set({
    View: [[WithView, { theme }]],
    FlexibleView: [['View', { sel: '.Flexible' }]],
    ListItemView: [['View', { sel: 'li' }]],
    ListView: [['View', { sel: 'ul' }]],
    ImageView: [['View', { sel: 'img' }]],
    Layout: [[WithLayout, { theme }]],
    NavLayout: [['Layout', { sel: 'nav' }]],
    AppNav: [['NavLayout', {
      sel: '.' + style(...Style(theme))
    }]],
    Link: [[WithLink, { theme }]],
  })),
  ({ Component }) => {

    const Layout = Component.get('Layout').make
    const WithFlexibleView = Component.get('FlexibleView')
    const FlexibleView = WithFlexibleView.make
    const AppNav = Component.get('AppNav').make
    const Link = Component.get('Link').make
    const Image = Component.get('ImageView').make
    const WithList = Component.get('ListView')
    const Item = Component.get('ListItemView').make


    return component => Component(component)
      .concat(AppNav({
        spaced: true,
        gutter: true,
        style: {
          paddingTop: 0,
          paddingBottom: 0,
        },
        has: [
          Link(
            Image({
              attrs: {
                src: 'logo.png'
              },
            })
          ),
          FlexibleView(),
          Layout({
            spaced: true,
            gutter: true,
            style: {
              justifyContent: 'flex-end',
              alignItems: 'center',
            },
          })
            .map(WithList([
              Item(Link({ href: '/', attrs: { href: '/' }, has: 'Home' })),
              Item(Link({ href: '/layouts', attrs: { href: '/layouts' }, has: 'Layouts' })),
              Item(Link({ href: '/forms', attrs: { href: '/forms' }, has: 'Forms' })),
            ]))
        ]
      }))
  }
)

module.exports = {
  default: WithAppNav,
  WithAppNav
}