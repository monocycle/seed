// const { Stream: $ } = require('xstream')
// const { footer } = require('@cycle/dom')
const pipe = require('ramda/src/pipe')
const tap = require('ramda/src/tap')
const { ensurePlainObj } = require('@monocycle/component/utilities/ensurePlainObj')
const { WithLayout } = require('@monocycle/dom/Layout')

// const Footer = () => ({ DOM: $.of(footer()) })

const WithAppFoot = pipe(
  ensurePlainObj,
  tap(({ Component: C }) => {

    C.set({
      Layout: WithLayout,
      BarLayout: [['Layout', { sel: '.Bar' }]],
    })
  }),
  ({ Component: C }) => {

    const BarLayout = C.get('BarLayout').make

    return component => C(component)
      .concat(Layout({
        direction: 'column',
        gutter: false,
        has: Router({
          Default: Layout({
            fill: true,
            has: 'This is home page'
          }),
          resolve: [
            {
              resolve: PathToRegexp('/layouts/:path?'),
              value: LayoutsPage()
            },
            {
              resolve: PathToRegexp('/forms/:path?'),
              value: Layout({
                spaced: true,
                fill: true,
                direction: 'column',
                has: [
                  H1('Forms'),
                  Layout({
                    style: {
                      justifyContent: 'center',
                      alignItems: 'center',
                    },
                    has: [
                      FlexibleView([

                        CardLayout({
                          has: [
                            P(`Components/Behaviors used:`),
                            List([
                              Item('Form'),
                              Item('FieldGroup'),
                              Item('Field'),
                              Item('Button'),
                              Item('Clickable'),
                              Item('Validable'),
                              Item('Focusable'),
                              Item('Codemirror'),
                              Item('Editor'),
                              Item('(to be continued)'),
                            ]),
                          ]
                        }).map(C.get('Layout', {
                          direction: 'column',
                          spaced: true,
                        })),

                        H2('Codemirror'),

                        CardLayout({
                          gutter: false,
                          has: [

                            '<Codemirror>'
                            // makeCodemirror().isolation({
                            //   DOM: 'Codemirror',
                            //   '*': null,
                            // })
                          ]
                        }),

                        H2('Editor'),

                        CardLayout({
                          gutter: false,
                          has: [
                            '<Editor>'
                            // makeEditor({ classes }).isolation({
                            //   DOM: 'Editor',
                            //   '*': null,
                            // })
                          ]
                        }),
                      ]).map(C.get('Layout', {
                        direction: 'column',
                        spaced: true
                      })),

                      FlexibleView({
                        has: [
                          H2('Contact form'),
                          CardLayout({
                            gutter: false,
                            has: FlexibleView({
                              sel: '.contact-form',
                              has: [
                                BarLayout({
                                  from: (sinks, sources) => sources.state.stream
                                    .map(prop('message'))
                                    .startWith('')
                                    .map(message => ({
                                      style: {
                                        justifyContent: 'center',
                                        display: !message ? 'none' : 'flex',
                                        backgroundColor: message && message.type === 'success'
                                          ? '#1bc876'
                                          : '#e93f3b',
                                        color: '#fefefe'
                                      },
                                      children: message
                                    }))
                                }),
                                // ContactForm({
                                //   key: 'contactForm'
                                // }),
                              ]
                            })
                          })
                        ]
                      }).map(C.get('Layout', {
                        direction: 'column',
                        spaced: true
                      })),
                    ]
                  }),
                ]
              })
            },
          ]
        })
      }))
  }
)

module.exports = {
  default: WithAppFoot,
  WithAppFoot
}