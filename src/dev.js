const insertRoot = require('./utilities/insertRoot')
// const { Start, initHMR } = require('./utilities/start')
// const { WithForm } = require('@monocycle/dom/Form')
// const log = require('@monocycle/component/utilities/log').Log('dev')

const { Style } = require('./style')
const { WithApp } = require('./components/App')
// const map = require('ramda/src/map')
const pipe = require('ramda/src/pipe')
const apply = require('ramda/src/apply')
const __ = require('ramda/src/__')

// const { WithLayoutsPage } = require('./components/App/pages/LayoutsPage')
// const { WithFormsPage } = require('./components/App/pages/FormsPage')
// const { WithContactForm } = require('./components/App/pages/FormsPage/ContactForm')
// const { WithCounter } = require('./components/Counter')
// const { WithRouterExample } = require('./components/RouterExample')
// const { WithGroupExample } = require('./components/GroupExample')
// const { WithRouter } = require('@monocycle/history/Router')
// const { defaultTheme } = require('./theme')
// const { WithSwitch, Case } = require('@monocycle/switch')
// const { WithTransition } = require('@monocycle/state/Transition')
// const { WithTextareaField } = require('@monocycle/dom/TextareaField')
// const { WithField } = require('@monocycle/dom/Field')
// const { WithForm } = require('@monocycle/dom/Form')

const { Drivers } = require('./drivers')
const { Log } = require('@monocycle/component/utilities/log')

const { WithSinkLogger } = require('@monocycle/dom/ViewLogger')


const { run } = require('@cycle/run')
const { withState } = require('@cycle/state')

insertRoot()

// const start = Start({
//   Drivers: Drivers.bind(void 0, { root: '.root' }),
//   Style,
//   Behavior: o => pipe(
//     // WithGroupExample(o),
//     // WithApp(o),
//     // WithForm(o),
//     WithContactForm(o),
//     // map(o.Component.get('Transition', void 0)),
//     WithSinkLogger({
//       sinkName: 'DOM',
//       log: Log({
//         scope: 'RENDER',
//         log: console.warn.bind(console)
//       }),
//     })
//   ),
//   // theme: defaultTheme,
//   state: {
//     // bu: { isBu: true },
//     // ga: -2,
//     // value: "ga"
//   },
//   debug: true,
//   dependencies: {
//     Transition: [[WithTransition, {
//       Log: name => Log(name)
//         .bind(void 0, `%c:`, ['color: #32b87c'].join(';')),
//     }]],
//   }
// })

// initHMR(module, true)

// start({
//   theme: {
//     colors: {
//       // light: 'blue'
//     }
//   }
// })



Style()

run(
  pipe(
    apply(__, []),
    apply(__, []),
    // WithTransition(void 0),
    withState,
    WithSinkLogger({
      sinkName: 'DOM',
      log: Log({
        scope: 'RENDER',
        log: console.debug.bind(console) // eslint-disable-line
      }),
    })
  )(WithApp),
  Drivers({
    root: '.root'
  }),
)
