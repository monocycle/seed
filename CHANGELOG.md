## 2.0.0 (2019-02-04)

* fix(seed): update @component ([1efe0a6](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/1efe0a6))
* chore(seed): add .gitignore ([b4f9cbc](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/b4f9cbc))
* chore(seed): add files ([10f0606](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/10f0606))


### BREAKING CHANGE

* yes


