
const Budo = require('budo')
const Url = require('url')


Budo('./src/dev.js', {
  pushstate: true,
  stream: process.stdout,
  live: '*.{html,css}',
  dir: ['src', 'public'],
  port: 8988,
  browserify: {
    plugin: 'browserify-hmr',
  },
  middleware: (req, response, next) => {

    if (req.method !== 'POST')
      return next()

    const url = Url.parse(req.url)

    if (url.pathname !== '/unicorn')
      return next()

    // res.statusCode = 400
    // res.end(JSON.stringify({
    //   statusCode: 400,
    //   message: 'invalid from'
    // }))

    response.writeHead(200, {
      'Content-Type': 'application/json',
    })
    response.write(JSON.stringify({
      message: 'Unicorn sent successfully'
    }))
    response.end()
  }
}).on('connect', (/* event */) => {

  console.log('connected') // eslint-disable-line
})
